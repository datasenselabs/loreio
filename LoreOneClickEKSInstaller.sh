#!/bin/bash

#Use Case  sh <name of the script> <name of key pair without .pem> <name-of-region-on-which-you-want-to-launch-EKS> 

if [ "${USER}" != "root" ]; then
  echo "$0 must be run as root!"
  exit 2
fi


# Part 1 Install Pre Requiste tool

while getopts c:i:r:a:s: option
do
case "${option}"
in
c) CLUSTERNAME=${OPTARG};;
i) KEYPAIR=${OPTARG};;
r) REGION=${OPTARG};;
a) ACCESSKEY=${OPTARG};;
s) SECRETKEY=${OPTARG};;
esac
done

echo -e "\e[1;31m Install & Configure Pre Requiste tool \e[0m"

#yum install -y git
#yum install -y awscli
#yum upgrade -y 
#aws configure
aws configure set aws_access_key_id $ACCESSKEY
aws configure set aws_secret_access_key $SECRETKEY
aws configure set default.region $REGION


# Part 2 Configure working directory of Lore IO Product Deployment

#echo -e "\e[1;32m Configure working directory of Lore IO Deployment \e[0m"
#git clone https://loreio_customer@bitbucket.org/datasenselabs/loreio.git


# Part 3 " Setup EKS cluster for Lore-IO Deployment"

#curl -LO https://storage.googleapis.com/kubernetes-release/release/$(curl -s https://storage.googleapis.com/kubernetes-release/release/stable.txt)/bin/linux/amd64/kubectl

cd

curl -o kubectl https://amazon-eks.s3-us-west-2.amazonaws.com/1.14.6/2019-08-22/bin/linux/amd64/kubectl

chmod +x ./kubectl

mv ./kubectl /usr/local/bin/

curl -o aws-iam-authenticator https://amazon-eks.s3-us-west-2.amazonaws.com/1.11.5/2018-12-06/bin/linux/amd64/aws-iam-authenticator

chmod +x ./aws-iam-authenticator

mkdir -p bin

cp ./aws-iam-authenticator $HOME/bin/aws-iam-authenticator && export PATH=$HOME/bin:$PATH

curl --silent --location "https://github.com/weaveworks/eksctl/releases/download/latest_release/eksctl_$(uname -s)_amd64.tar.gz" | tar xz -C /tmp

mv /tmp/eksctl /usr/local/bin/

#remove .pem from the keypair

export keypair=${KEYPAIR%%.*}

eksctl create cluster --name=$CLUSTERNAME  --nodes=1  --ssh-access --ssh-public-key=$keypair --region=$REGION --node-type=t2.2xlarge --node-volume-size=150
# Varify cluster readiness

sleep 20

kubectl get nodes

echo -e "\e[1;35m Enable Cluster Logging \e[0m" 

eksctl utils update-cluster-logging --region=$REGION --name=$CLUSTERNAME --enable-types=all --approve



#-----------------------------------------------------------------------------------------

# Open k8n cluster dashboard from local machine browser

echo -e "\e[1;36m  Setup k8n cluster dashboard from local macllhine browser \e[0m"

kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

echo -e "\e[1;33m Deploying heapster to enable container cluster monitoring and performance analysis on your cluster \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml

echo -e "\e[1;33m Deploy the influxdb backend for heapster to your cluster \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml

echo -e "\e[1;33m Create the heapster cluster role binding for the dashboard \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml


echo -e "\e[1;33m create the admin service account and cluster role binding \e[0m"

cat << EOF > admin-service-account.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF

kubectl apply -f admin-service-account.yaml



echo -e "\e[1;36m starting dashboard services \e[0m"

kubectl proxy &

sleep 5

echo -e "\e[1;33m In order to login in dashboard please copy below <authentication_token> value from the output and follow steps in monitoring document \e[0m"

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')


echo -e "\e[1;34m Setting up Inbound rule to access application ports  \e[0m"

export newlyClusterName=$(eksctl get cluster | awk 'NR == 2 {print $1}') # get clustername
export securityGroup=$(eksctl get cluster -n $newlyClusterName | awk 'NR == 2 {print $7}')  # security group

# update inbount rules for App access

aws ec2 authorize-security-group-ingress --group-id $securityGroup --protocol tcp --port 22 --cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress --group-id $securityGroup --protocol tcp --port 31079-31080 --cidr 0.0.0.0/0

aws ec2 authorize-security-group-ingress --group-id $securityGroup --protocol tcp --port 80 --cidr 0.0.0.0/0



echo -e "\e[1;34m Lore Io respective DBs setup on worker node \e[0m"

#first time setup
cp /home/centos/$KEYPAIR /root/

chmod 400 $KEYPAIR

cp /home/centos/$KEYPAIR /root/

chmod 400 $KEYPAIR


#export KEYPAIR=VelotioDevOps

#whenever new node launch below command need to be execute

export privateDNSIP=$(kubectl get node| awk 'NR == 2 {print $1}')

export nodePublicIP=$(aws ec2 describe-instances --filters "Name=private-dns-name,Values=$privateDNSIP" --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)

#export pemextension=.pem


ssh -i $KEYPAIR -oStrictHostKeyChecking=no ec2-user@$nodePublicIP -t  "sudo mkdir -p /mnt/{mongo,es,postgres,mysql}"



echo -e "\e[1;31m EKS k8n's Cluster , K8n Dashboard & WorkerNode setup is ready for application deployment > Starting Now  \e[0m"

#switching branch automatically till the time its merge in to master

cd loreio/on-prem

#git checkout feature.k8n.EKS

#git pull origin feature.k8n.EKS

make submit-pvc

sleep 60

make submit-app

sleep 60

echo -e "\e[1;31m Validating Lore-Io application deployment status  \e[0m"

kubectl get po -o wide




