---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $APP_INSTANCE_NAME-ui
  namespace: $NAMESPACE
  labels: &UIDeploymentLabels
    app.kubernetes.io/name: $APP_INSTANCE_NAME
    app.kubernetes.io/component: lore-app-ui
spec:
  selector:
    matchLabels: *UIDeploymentLabels
  replicas: 1
  template:
    metadata:
      labels: *UIDeploymentLabels
    spec:
      imagePullSecrets:
      - name: regcred
      containers:
      - image: $UI_IMAGE
        imagePullPolicy: Always
        name: $APP_INSTANCE_NAME-ui
        args:
        - -c
        - chown -R www-data:www-data /var/cache/nginx && nginx -c /etc/nginx/nginx.conf
        command:
        - sh
        env:
        - name: MY_HOST
          valueFrom:
            configMapKeyRef:
              key: MY_HOST
              name: $APP_INSTANCE_NAME-restapi-config
        - name: ENVIRONMENT
          valueFrom:
            configMapKeyRef:
              key: ENVIRONMENT
              name: $APP_INSTANCE_NAME-restapi-config
        ports:
        - name: http
          containerPort: 8080
        volumeMounts:
        - name: nginx-config
          mountPath:  /etc/nginx/sites-enabled/nginx_upstream.conf
          subPath: nginx_upstream.conf
          readOnly: true
        readinessProbe:
          httpGet:
            path: /nginx_status
            port: 8080
          initialDelaySeconds: 60
          periodSeconds: 60
        livenessProbe:
          httpGet:
            path: /
            port: 8080
          initialDelaySeconds: 60
          periodSeconds: 60
      volumes:
      - name: nginx-config
        configMap:
          name: $APP_INSTANCE_NAME-config
          items:
          - key: nginx_upstream.conf
            path: nginx_upstream.conf

---
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/component: lore-app
    app.kubernetes.io/name: $APP_INSTANCE_NAME
  name: lore-ui
  namespace: default
spec:
  externalTrafficPolicy: Cluster
  ports:
   - nodePort: 31080
     port: 8080
     protocol: TCP
     targetPort: 8080
  selector:
    app.kubernetes.io/component: lore-app-ui
    app.kubernetes.io/name: $APP_INSTANCE_NAME
  type: LoadBalancer