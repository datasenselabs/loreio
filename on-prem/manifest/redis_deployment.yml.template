---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $APP_INSTANCE_NAME-redis
  namespace: $NAMESPACE
  labels: &RedisDeploymentLabels
    app.kubernetes.io/name: $APP_INSTANCE_NAME
    app.kubernetes.io/component: lore-db-redis
spec:
  selector:
    matchLabels: *RedisDeploymentLabels
  replicas: 1
  template:
    metadata:
      labels: *RedisDeploymentLabels
    spec:
      containers:
      - image: $REDIS_IMAGE
        imagePullPolicy: IfNotPresent
        name: $APP_INSTANCE_NAME-redis
        # env:
        # - name: REDIS_HOST
        #   value: $APP_INSTANCE_NAME-mysql-svc
        ports:
        - name: http
          containerPort: 6379
        readinessProbe:
          exec:
            command: ["sh", "-c", "exec redis-cli info"]
          initialDelaySeconds: 15
        livenessProbe:
          exec:
            command: ["sh", "-c", "exec redis-cli info"]
          initialDelaySeconds: 60
---
---
apiVersion: v1
kind: Service
metadata:
  labels:
    app.kubernetes.io/component: lore-app
    app.kubernetes.io/name: $APP_INSTANCE_NAME
  name: lore-redis
  namespace: default
spec:
  externalTrafficPolicy: Cluster
  ports:
   - nodePort: 31079
     port: 6379
     protocol: TCP
     targetPort: 6379
  selector:
    app.kubernetes.io/component: lore-db-redis
    app.kubernetes.io/name: $APP_INSTANCE_NAME
  type: NodePort