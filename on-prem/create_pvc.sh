#!bash

export APP_INSTANCE_NAME=${APP_INSTANCE_NAME}
#export MY_IP=$(ip route get 8.8.8.8 | awk -F"src " 'NR==1{split($2,a," ");print a[1]}') # for platform9 installer only

if [ "$setuptype" = "selfmanaged" ];
	then 
		export MY_IP=$(hostname)  # for v1.15 new self managed installer only
	else
        export MY_IP=$(kubectl get node -o=jsonpath={.items[0].metadata.name} | grep "")   # for EKS only
fi

#export MY_IP=$(hostname)  # for v1.15 new self managed installer only
#export MY_IP=$(kubectl get node -o=jsonpath={.items[0].metadata.name} | grep "")   # for EKS only

awk 'FNR==1 {print "---"}{print}' pvc/*.yml pvc/*.yml.template  | \
   envsubst '$APP_INSTANCE_NAME $MY_IP' \
   > ${APP_INSTANCE_NAME}_sc_manifest.yaml

kubectl apply --dry-run -f ${APP_INSTANCE_NAME}_sc_manifest.yaml

kubectl create namespace $NAMESPACE  || true
kubectl apply -f ${APP_INSTANCE_NAME}_sc_manifest.yaml

