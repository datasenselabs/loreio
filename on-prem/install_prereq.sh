#!/bin/bash

if [ "${USER}" != "root" ]; then
	echo "$0 must be run as root!"
	exit 2
fi

if [ -f /etc/redhat-release ]; then
	set -x
  subscription-manager repos --enable=rhel-7-server-extras-rpms
	yum upgrade -y
	yum install -y docker
	setenforce 0
	sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
	swapoff -a
	modprobe br_netfilter
	modprobe vxlan # Needed for CNI

# Docker Installation Command :
	echo "Docker Installation Command"

	yum install -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.74-1.el7.noarch.rpm
	yum upgrade -y
	yum install -y docker-ce
	systemctl start docker
	systemctl enable docker

    cat <<EOF >  /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF
    sysctl --system

    cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=1
repo_gpgcheck=1
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

	setenforce 0
	#yum install -y kubelet kubeadm kubectl kubernetes-cni wget
	yum install -y kubelet-1.19.0 kubeadm-1.19.0 kubectl-1.19.0 wget

fi
systemctl enable kubelet
systemctl start kubelet


kubeadm init --pod-network-cidr=192.168.0.0/24

sleep 15

echo "update k8n local network port"
ex -s -c '38i|    - --service-node-port-range=80-32767'  -c x /etc/kubernetes/manifests/kube-apiserver.yaml

sleep 40


kubectl --kubeconfig=/etc/kubernetes/admin.conf taint nodes --all node-role.kubernetes.io/master-kubectl taint nodes --all node-role.kubernetes.io/master-

#install -o 1000 -d /home/$(id -nu 1000)/.kube
#install -o 1000 /etc/kubernetes/admin.conf /home/$(id -nu 1000)/.kube/config

sleep 15





# Kubernetes post install commands:
echo "Kubernetes post install commands"
mkdir -p $HOME/.kube && sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config && sudo chown $(id -u):$(id -g) $HOME/.kube/config


# Adding kubernetes command set to the OS path:
echo "Adding kubernetes command set to the OS path"
sed -i '/$PATH/d' ~/.bash_profile && sed -i '9iPATH=/opt/bin:$PATH:$HOME/bin' ~/.bash_profile



#echo "deploy flannel network"

#kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml

# Apply the weave-net container network
kubectl apply -f "https://cloud.weave.works/k8s/net?k8s-version=$(kubectl version | base64 | tr -d '\n')"

sleep 10


kubectl get node

echo "node deployment completed"

# Open k8n cluster dashboard from local machine browser

echo -e "\e[1;36m  Setup k8n cluster dashboard from local machine browser \e[0m"

kubectl create -f https://raw.githubusercontent.com/kubernetes/dashboard/v1.10.1/src/deploy/recommended/kubernetes-dashboard.yaml

echo -e "\e[1;33m Deploying heapster to enable container cluster monitoring and performance analysis on your cluster \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/heapster.yaml

echo -e "\e[1;33m Deploy the influxdb backend for heapster to your cluster \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/influxdb/influxdb.yaml

echo -e "\e[1;33m Create the heapster cluster role binding for the dashboard \e[0m"

kubectl apply -f https://raw.githubusercontent.com/kubernetes/heapster/master/deploy/kube-config/rbac/heapster-rbac.yaml


echo -e "\e[1;33m create the admin service account and cluster role binding \e[0m"

cat << EOF > admin-service-account.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  name: admin-user
  namespace: kube-system
---
apiVersion: rbac.authorization.k8s.io/v1beta1
kind: ClusterRoleBinding
metadata:
  name: admin-user
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: cluster-admin
subjects:
- kind: ServiceAccount
  name: admin-user
  namespace: kube-system
EOF

kubectl apply -f admin-service-account.yaml



echo -e "\e[1;36m starting dashboard services \e[0m"

kubectl proxy &

sleep 5

echo -e "\e[1;33m In order to login in dashboard please copy below <authentication_token> value from the output and follow steps in monitoring document \e[0m"

kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep admin-user | awk '{print $1}')

