#!bash

export APP_INSTANCE_NAME=${APP_INSTANCE_NAME}
export NAMESPACE=${NAMESPACE}
export CDH_DOCKER_IP=192.168.0.1
export UI_IMAGE=$REGISTRY/ui:$TAG
export BE_IMAGE=$REGISTRY/restapi:$TAG
export REDIS_IMAGE=redis:latest
export MYSQL_IMAGE=mysql:latest
export POSTGRES_IMAGE=postgres:11.0
export MONGO_IMAGE=mongo:3.2.1
export ES_IMAGE=$REGISTRY/es:prod
export DJANGO_MONGO_USER=django-user
export DJANGO_MYSQL_USER=django-user
export DJANGO_MYSQL_USER_DB=user_db
export POSTGRES_USER=django-user
export CACHE_ENABLE="TRUE"

awk 'FNR==1 {print "---"}{print}' manifest/*.yml.template  | \
   envsubst '$APP_INSTANCE_NAME $NAMESPACE $CDH_DOCKER_IP $ES_IMAGE $MYSQL_IMAGE $POSTGRES_IMAGE $MONGO_IMAGE $REDIS_IMAGE $BE_IMAGE $UI_IMAGE $DJANGO_MONGO_USER $DJANGO_MYSQL_USER $DJANGO_MYSQL_USER_DB $POSTGRES_USER $CACHE_ENABLE' \
   > ${APP_INSTANCE_NAME}_manifest.yaml

kubectl create namespace $NAMESPACE  || true
kubectl apply -f ${APP_INSTANCE_NAME}_manifest.yaml


