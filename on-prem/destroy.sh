#!/bin/bash

# Uninstall kubernates 

if [ "${USER}" != "root" ]; then
	echo "$0 must be run as root!"
	exit 2
fi

export mynode=$(hostname)


echo "delete master node "

kubectl delete node --all
kubectl drain $mynode 


echo "remove kubeadm completely"
yum remove -y yum-utils device-mapper-persistent-data lvm2
kubeadm reset

sleep 20 



echo "The reset process does not reset or clean up iptables rules , hence reseting iptables too"

iptables -F && iptables -t nat -F && iptables -t mangle -F && iptables -X



sleep 20




# Uninstall  Docker ( required because both script(old & new are using different versions)

echo "Docker completely remove started"

yum-config-manager  --disable repository     https://download.docker.com/linux/centos/docker-ce.repo

yum remove -y http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.74-1.el7.noarch.rpm

rpm -e  http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.74-1.el7.noarch.rpm

rpm -i  http://mirror.centos.org/centos/7/extras/x86_64/Packages/container-selinux-2.74-1.el7.noarch.rpm


echo "Removing docker binaries..."
rm -f /usr/local/bin/docker
rm -f /usr/local/bin/docker-machine
rm -f /usr/local/bin/docker-compose



echo "Removing boot2docker.iso"
rm -rf /usr/local/share/boot2docker


echo "Deleting All docker Images"
#ensure fresh installtion

docker rmi $(docker images -a -q)
# Reclaim space by clearing up docker volume
docker volume prune -f

yum remove -y docker-ce docker-selinux
yum remove  -y docker-ce-cli

                  
rm -rf /var/lib/docker
rm -rf /var/lib/docker-engine/
rm -rf /etc/docker/
rm -rf /var/run/docker.sock
rm -rf /run/docker/
rm -rf /usr/libexec/docker


yum remove -y kubeadm kubectl kubelet kubernetes-cni kube*
yum clean all

rm -rf /opt/bin/kubectl kubelet kubeadm
rm -rf ~/.kube
rm -rf /var/lib/etcd
cd /root/loreio/on-prem
rm -rf docker
cd


echo "All Done!"

