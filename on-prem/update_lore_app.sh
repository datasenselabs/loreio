#!/usr/bin/env bash

for DEPLOYMENT in 'cust-restapi' 'cust-streamapi' 'cust-ui'
do
	kubectl patch deployment $DEPLOYMENT -p "{\"spec\":{\"template\":{\"metadata\":{\"annotations\":{\"date\":\"`date +'%s'`\"}}}}}"
	kubectl rollout status deployment/$DEPLOYMENT
done
