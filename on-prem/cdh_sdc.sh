#!/usr/bin/env bash                                  #!/usr/bin/env bash
yum install wget -y
wget -N https://archive.cloudera.com/cm6/6.2.0/redhat7/yum/cloudera-manager.repo -P  /etc/yum.repos.d/
rpm --import https://archive.cloudera.com/cm6/6.2.0/redhat7/yum/RPM-GPG-KEY-cloudera
yum install java-1.8.0-openjdk -y
yum install cloudera-manager-daemons cloudera-manager-agent cloudera-manager-server -y

yum install mariadb-server -y
systemctl enable mariadb
systemctl start mariadb


wget -N https://dev.mysql.com/get/Downloads/Connector-J/mysql-connector-java-5.1.46.tar.gz
tar zxvf mysql-connector-java-5.1.46.tar.gz
mkdir -p /usr/share/java/
cd mysql-connector-java-5.1.46
cp mysql-connector-java-5.1.46-bin.jar /usr/share/java/mysql-connector-java.jar

mysqladmin password 'root'
for VARIABLE in 'scm' 'amon' 'rman' 'hue' 'hive' 'sentry' 'nav' 'navms' 'oozie'
do
	mysql -e "CREATE DATABASE $VARIABLE DEFAULT CHARACTER SET utf8 DEFAULT COLLATE utf8_general_ci;" -proot
done

/opt/cloudera/cm/schema/scm_prepare_database.sh mysql scm root ----scm-password-script root


cd /opt/cloudera/csd
wget -nc https://archives.streamsets.com/datacollector/3.9.0/csd/STREAMSETS-3.9.0.jar
cd /opt/cloudera/parcel-repo
wget -N https://archives.streamsets.com/datacollector/3.9.0/parcel/STREAMSETS_DATACOLLECTOR-3.9.0-el7.parcel.sha
wget -N https://archives.streamsets.com/datacollector/3.9.0/parcel/STREAMSETS_DATACOLLECTOR-3.9.0-el7.parcel



systemctl start cloudera-scm-server
systemctl start cloudera-scm-agent