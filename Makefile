TAG=$(shell date +"%Y-%m-%d-%H-%M-%S")

export CONTEXT=lore${USER}
export USER
export VERSION?=prod
export MYLOCATION=$(shell pwd)
export USERID=$(shell id -u)
export GROUPID=$(shell id -g)

########START AND RESTART ENVIRONMENT

configure: configure-env init-dbs

configure-env:
	bash scripts/configure_env.sh

configure-hive:
	bash scripts/configure_hive.sh

init-dbs:
	bash scripts/init_dbs.sh

check:
	mkdir -p data/mysql
	mkdir -p conf
	mkdir -p logs/data/cuberon/logs/query
	mkdir -p logs/django
	mkdir -p logs/uwsgi
	touch logs/uwsgi/uwsgi.log
	chmod 750 -R data/mysql

restart: export ENVIRONMENT=prod
restart: recover-file-ownership clean check 
	date +"%FT%T%z"
	docker-compose -p ${CONTEXT} -f config/deploy.yml stop
	docker-compose -p ${CONTEXT} -f config/deploy.yml rm -f
	docker-compose -p ${CONTEXT} -f config/deploy.yml up -d --force-recreate ui

restart-sdc:
	date +"%FT%T%z"
	docker-compose -p ${CONTEXT} -f config/deploy.yml stop sdc
	docker-compose -p ${CONTEXT} -f config/deploy.yml rm -f sdc
	docker-compose -p ${CONTEXT} -f config/deploy.yml up -d --force-recreate sdc

pull:
	docker pull mongo
	docker pull mysql
	docker pull redis
	docker pull loreio/ui:${VERSION}
	docker pull loreio/restapi:${VERSION}
	docker pull loreio/sdc
	docker pull loreio/es:${VERSION}
	docker pull loreio/impala:${VERSION}

statusimpala:
	-docker exec -it ${CONTEXT}_quickstart.cloudera_1 sh -c 'for x in `cd /etc/init.d ; ls hadoop-*` ; do sudo service $$x status ; done'

stopimpala:
	-docker exec -it ${CONTEXT}_quickstart.cloudera_1 sh -c 'for x in `cd /etc/init.d ; ls hadoop-*` ; do sudo service $$x stop ; done'

killall: stopimpala
#	docker ps -q | xargs  --no-run-if-empty docker kill
	-docker ps -aq --filter "name=${CONTEXT}_*" | awk '{print $$1}' | xargs --no-run-if-empty docker kill

cleani:
	-docker images -q -f dangling=true | xargs --no-run-if-empty docker rmi

cleanc:
	-docker ps -aq --filter "name=${CONTEXT}_*" | awk '{print $$1}' | xargs --no-run-if-empty docker rm

clean: cleanc cleani

show-lore-services:
	-docker ps -a --filter "name=${CONTEXT}_*"

recover-file-ownership: killall clean
	-docker run -t --name ${CONTEXT}_mongo -v ${MYLOCATION}:/loredata mongo sh -c 'chown ${USERID}:${GROUPID} -R /loredata'
