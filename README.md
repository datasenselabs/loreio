# README #
Used to install Lore IO

### Download Installer ###
- Setup SSH Keys https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html
- Copy your Public Key `cat ~/.ssh/id_rsa.pub`
- Send key to your Lore IO Contact who can add it to access keys at https://bitbucket.org/datasenselabs/loreio/admin/access-keys/
- Get the installation code
- Installation code can also be emailed/shipped

```
$ cd ~
$ git clone git@bitbucket.org:datasenselabs/loreio.git
```

### Authorize a Dockerhub account ###

- Create an account for the customer on https://hub.docker.com/
- Ask your Lore IO Contact to authorize that account
- From the machine with the correct user do `docker login`

### Set permissions
- chmod 777 -R data/mysql
- chmod 777 -R data/cdh/


### Setup environment and dataset ###

- make configure-env
- make init-dbs

### Run or manage the services ##
- `make restart` from the loreio directory will run the service

### Access Lore from 

### Credentials
###https://www.cloudera.com/documentation/enterprise/latest/topics/quickstart_vm_administrative_information.html#xd_583c10bfdbd326ba-3ca24a24-13d80143249--7f88
- Mongo:    django-user:djpasswd
- Cloudera Hue: cloudera:cloudera
- Cloudera Mysql: root:cloudera
- 

### Safely Stopping Impala services
- docker exec -it loreio_quickstart.cloudera_1 bash
  - for x in `cd /etc/init.d ; ls hadoop-*` ; do sudo service $x stop ; done
- To leave safemode upon restart
  - sudo -u hdfs hadoop dfsadmin -safemode leave
- To start/stop/status of cloudera manager
  - sudo /home/cloudera/cloudera-manager --express


### Test impala connection from python
 - #python
   -#> from impala.dbapi import connect
   -#> conn = connect(host='quickstart.cloudera', port=21050, user='cloudera', password='cloudera')
   -#> cur = conn.cursor()
   -#> cur.execute('show tables')