set +e
cp config/deploy_template.yml config/deploy.yml
set -e

export set ENVIRONMENT="prod"
export set RUN_ENVIRONMENT="prod"
export set DB_ENVIRONMENT="prod"
export set USER=`whoami`
export set MYLOCATION=`pwd`
export set VERSION="prod"
export set MYSQL_ROOT_PASSWORD="rtpasswd"
export set MYSQL_USER="django-user"
export set MYSQL_PWD="djpasswd"
export set MYSQL_USER_DB="user_db"
export set MONGO_USER="django-user"
export set MONGO_PWD="djpasswd"
export set LOREPORT="8080"
export set CDHPORT="7180"
export set HUEPORT="8888"
export set SDCPORT="18630"
export set USERID=`id -u`
export set GROUPID=`id -g`

echo "" > config/env.sh

for VAR in "ENVIRONMENT" "DB_ENVIRONMENT" "RUN_ENVIRONMENT" "USER" "MYLOCATION" "VERSION" "MYSQL_ROOT_PASSWORD" "MYSQL_USER" "MYSQL_PWD" "MYSQL_USER_DB" "MONGO_USER" "MONGO_PWD" "USERID" "GROUPID" "LOREPORT" "CDHPORT" "HUEPORT" "SDCPORT"
do
  if [ "$VAR" == "VERSION" -o "$VAR" == "LOREPORT" -o "$VAR" == "CDHPORT" -o "$VAR" == "HUEPORT" -o "$VAR" == "SDCPORT" ]; then
    read -p "Do you want to change $VAR (${!VAR}) to " new_value
    if [ ! -z "${new_value}" -a "${new_value}" != " " ]; then
      export set ${VAR}=${new_value}
    fi
  fi
  sed -i -e "s@\${${VAR}}@${!VAR}@g" config/deploy.yml
  echo "export set ${VAR}='${!VAR}'" >> config/env.sh
done

set -e


