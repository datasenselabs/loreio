
set +e
# Database connection configuration
export set CLUSTER_HOST="quickstart.cloudera"
export set CLUSTER_PORT=21050
export set CLUSTER_USER="cloudera"
export set CLUSTER_PASSWORD="cloudera"
export set DB_CONNECTOR_TYPE="IMPALA"
export set AUTH_MECHANISM="NOSASL"


for VAR in "CLUSTER_HOST" "CLUSTER_PORT" "CLUSTER_USER" "CLUSTER_PASSWORD" "DB_CONNECTOR_TYPE" "AUTH_MECHANISM"
do
  read -p "Do you want to change $VAR (${!VAR}) to " new_value
  if [ ! -z "${new_value}" -a "${new_value}" != " " ]; then
    export set ${VAR}=${new_value}
  fi
done


echo '

# Default settings used for project api
DEFAULT_DB_CONNECTOR_TYPE = "'${DB_CONNECTOR_TYPE}'"
DEFAULT_DB_CONNECTOR_SETTINGS = {
    "_cls": "DBConnectorSettings",
    "host" : "'${CLUSTER_HOST}'",
    "port" : '${CLUSTER_PORT}',
    "user" : "'${CLUSTER_USER}'",
    "password" : "'${CLUSTER_PASSWORD}'",
    "protocol" : "hiveserver2",
    "auth_mechanism" : "'${AUTH_MECHANISM}'",
    "kerberos_service_name": "hive"
}
' > ./config/schema_defaults.conf




