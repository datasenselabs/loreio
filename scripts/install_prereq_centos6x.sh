
# STEP-1: Install docker tools
set +e
yum install epel-release
# or download the epel-release manually
# rpm -iUvh http://dl.fedoraproject.org/pub/epel/7/x86_64/epel-release-6-8.noarch.rpm
set -e
yum update
yum install docker-io
curl -L https://github.com/docker/compose/releases/download/1.3.1/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

# STEP-2: Create docker user group
getent group docker || groupadd docker

# STEP-3: Add Lore IO user and add the user to docker group
set +e
adduser loreio -p uiojlfksadj$%#!
set -e
usermod -aG docker loreio

# STEP-4: start docker
service docker restart

## Choose STEP-5 if Internet is reachable else choose STEP-6

# STEP-5: Login to docker
# docker login

# STEP-6: Load Lore IO images
#docker load -i loreio_mongo
#docker load -i loreio_redis
#docker load -i loreio_es
#docker load -i loreio_ui
#docker load -i loreio_restapi
#docker load -i loreio_streamapi
