set +e
apt-get remove --purge -y git docker-engine make
set -e
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb https://apt.dockerproject.org/repo ubuntu-trusty main"
apt-get update
apt-get install -y --force-yes git docker-engine make

curl -L https://github.com/docker/compose/releases/download/1.6.2/docker-compose-`uname -s`-`uname -m` > /tmp/dcompose
set +e
rm /usr/local/bin/docker-compose
set -e
mv /tmp/dcompose /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose

adduser loreio
usermod -aG docker loreio
restart docker
docker login
