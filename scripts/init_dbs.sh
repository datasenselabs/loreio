set +e

source config/env.sh
CONTEXT="lore${USER}"

make recover-file-ownership check

docker-compose -p ${CONTEXT} -f config/deploy.yml up -d mysql
docker-compose -p ${CONTEXT} -f config/deploy.yml up -d mongo
sleep 30
docker exec -it ${CONTEXT}_mongo_1 mongo admin --eval 'db.createUser({ user: "'${MONGO_USER}'", pwd: "'${MONGO_PWD}'", roles: [ { role: "root", db: "admin"} ] })'
docker exec -it ${CONTEXT}_mongo_1 mongo medb --eval 'db.users_mongo.update({"username":"admin"}, {$set: {"_cls" : "UsersMongo", "username" : "admin", "access_level" : 0}},  { "upsert": true })'

set +e
docker-compose -p ${CONTEXT} -f config/deploy.yml run restapi sh -c 'python manage.py collectstatic --noinput && python manage.py migrate; echo "from django.contrib.auth.models import User; User.objects.create_superuser(\"admin\", \"admin@example.com\", \"adminpass\")" | python manage.py shell;'
docker-compose -p ${CONTEXT} -f config/deploy.yml run restapi sh -c 'echo "from django.contrib.auth.models import User; from oauth2_provider.models import Application; Application.objects.create(name=\"LoreUI\", client_id=\"5UeBZCatstf5m0uNL6bqjkgJ78nKwNy4usjYGlCt\", client_secret=\"AcMk4b5jQjy64x5YYZ3D2aDmGEdWg0KrxNUHIbNWxQlC6Ze9s5dWjjX9TmDSlfKkuonKhFsBgGJmTZxeqNY2838rFypheL3KpzK0QzAfoU8Jje6zeJI4iExwH0KdNzrr\", client_type=\"confidential\", authorization_grant_type=\"password\", user=User.objects.filter(username=\"admin\")[0] ) " | python manage.py shell; '
set -e



