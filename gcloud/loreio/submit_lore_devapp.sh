#!bash

export APP_INSTANCE_NAME=${APP_INSTANCE_NAME}
export NAMESPACE=${NAMESPACE}

export UI_IMAGE=$REGISTRY/ui:$TAG
export BE_IMAGE=$REGISTRY/restapi:$TAG
export REDIS_IMAGE=$REGISTRY/redis:$TAG
export MYSQL_IMAGE=$REGISTRY/mysql:$TAG
export MONGO_IMAGE=$REGISTRY/mongo:$TAG
export ES_IMAGE=$REGISTRY/es:$TAG
export DJANGO_MONGO_USER=django-user
export DJANGO_MYSQL_USER=django-user
export DJANGO_MYSQL_USER_DB=user_db
export CACHE_ENABLE="TRUE"

awk 'FNR==1 {print "---"}{print}' dev_manifest/*.yml.template  | \
   envsubst '$APP_INSTANCE_NAME $NAMESPACE $ES_IMAGE $MYSQL_IMAGE $MONGO_IMAGE $REDIS_IMAGE $BE_IMAGE $UI_IMAGE $DJANGO_MONGO_USER $DJANGO_MYSQL_USER $DJANGO_MYSQL_USER_DB $POSTGRES_USER $CACHE_ENABLE' \
   > ${APP_INSTANCE_NAME}_manifest.yaml

kubectl apply --dry-run -f ${APP_INSTANCE_NAME}_manifest.yaml

kubectl create namespace $NAMESPACE  || true
kubectl apply -f ${APP_INSTANCE_NAME}_manifest.yaml