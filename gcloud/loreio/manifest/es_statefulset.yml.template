---
apiVersion: apps/v1
kind: StatefulSet
metadata:
  name: $APP_INSTANCE_NAME-es
  namespace: $NAMESPACE
  labels: &esStatefulSetLabels
    app.kubernetes.io/name: $APP_INSTANCE_NAME
    app.kubernetes.io/component: lore-db-es
spec:
  replicas: 1
  selector:
    matchLabels: *esStatefulSetLabels
  serviceName: $APP_INSTANCE_NAME-es-svc
  updateStrategy:
    type: RollingUpdate
  template:
    metadata:
      labels: *esStatefulSetLabels
    spec:
      containers:
      - image: $ES_IMAGE
        name: es
        ports:
        - name: es
          containerPort: 9200
        volumeMounts:
        - name: $APP_INSTANCE_NAME-es-pvc
          mountPath: /usr/share/elasticsearch/data
          subPath: data
        - name: es-config
          mountPath:  /usr/share/elasticsearch/config
          readOnly: true
        readinessProbe:
          httpGet:
            path: _cluster/health
            port: 9200
          initialDelaySeconds: 60
        livenessProbe:
          httpGet:
            path: _cluster/health
            port: 9200
          initialDelaySeconds: 60
      volumes:
      - name: es-config
        configMap:
          name: $APP_INSTANCE_NAME-config
          items:
          - key: elasticsearch.yml
            path: elasticsearch.yml
  volumeClaimTemplates:
  - metadata:
      name: $APP_INSTANCE_NAME-es-pvc
      labels:
        app.kubernetes.io/name: $APP_INSTANCE_NAME
        app.kubernetes.io/component: lore-db-es
    spec:
      accessModes: [ "ReadWriteOnce" ]
      storageClassName: standard
      resources:
        requests:
          storage: 5Gi