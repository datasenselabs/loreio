---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: $APP_INSTANCE_NAME-streamapi
  namespace: $NAMESPACE
  labels: &StreamAPIDeploymentLabels
    app.kubernetes.io/name: $APP_INSTANCE_NAME
    app.kubernetes.io/component: lore-app-streamapi
spec:
  selector:
    matchLabels: *StreamAPIDeploymentLabels
  replicas: 1
  template:
    metadata:
      labels: *StreamAPIDeploymentLabels
    spec:
      containers:
      - image: $BE_IMAGE
        imagePullPolicy: IfNotPresent
        name: $APP_INSTANCE_NAME-streamapi
        stdin: true
        tty: true
        args:
        - bash
        lifecycle:
          postStart:
            exec:
              command: ["/bin/sh", "-c", "mkdir -p /logs/data/cuberon/logs/query && mkdir -p /var/log/django/"]
        env:
        - name: POSTGRES_HOST
          value: $APP_INSTANCE_NAME-postgres-svc.$NAMESPACE
        - name: POSTGRES_PORT
          value: "5432"
        - name: MYSQL_HOST
          value: $APP_INSTANCE_NAME-mysql-svc.$NAMESPACE
        - name: MYSQL_PORT
          value: "3306"
        - name: MONGO_HOST
          value: $APP_INSTANCE_NAME-mongo-svc.$NAMESPACE
        - name: MONGO_PORT
          value: "27017"
        - name: BIGMONGO_HOST
          value: $APP_INSTANCE_NAME-mongo-svc.$NAMESPACE
        - name: ELASTICSEARCH_HOST
          value: $APP_INSTANCE_NAME-es-svc.$NAMESPACE
        - name: ELASTICSEARCH_PORT
          value: "9200"
        - name: REDIS_HOST
          value: $APP_INSTANCE_NAME-redis-svc.$NAMESPACE
        - name: REDIS_PORT
          value: "6379"
        - name: SERVER_URL
          value: http://$APP_INSTANCE_NAME-restapi-svc.$NAMESPACE:9000/
        - name: MONGO_USER
          valueFrom:
            configMapKeyRef:
              key: DJANGO_MONGO_USER
              name: $APP_INSTANCE_NAME-restapi-config
        - name: MONGO_PWD
          valueFrom:
            secretKeyRef:
              key: django_mongo_password
              name: $APP_INSTANCE_NAME-mysql-secret
        - name: MYSQL_USER
          valueFrom:
            configMapKeyRef:
              key: DJANGO_MYSQL_USER
              name: $APP_INSTANCE_NAME-restapi-config
        - name: MYSQL_PWD
          valueFrom:
            secretKeyRef:
              key: django_mysql_password
              name: $APP_INSTANCE_NAME-mysql-secret
        - name: MYSQL_USER_DB
          valueFrom:
            configMapKeyRef:
              key: DJANGO_MYSQL_USER_DB
              name: $APP_INSTANCE_NAME-restapi-config
        - name: MY_HOST
          valueFrom:
            configMapKeyRef:
              key: MY_HOST
              name: $APP_INSTANCE_NAME-restapi-config
        - name: ENVIRONMENT
          valueFrom:
            configMapKeyRef:
              key: ENVIRONMENT
              name: $APP_INSTANCE_NAME-restapi-config
        - name: POSTGRES_USER
          valueFrom:
            configMapKeyRef:
              key: POSTGRES_USER
              name: $APP_INSTANCE_NAME-restapi-config
        - name: POSTGRES_PASSWORD
          valueFrom:
            secretKeyRef:
              key: postgres_password
              name: $APP_INSTANCE_NAME-postgres-secret
        volumeMounts:
        - name: bigquery-config
          mountPath:  /datasense/conf/schema_defaults.conf
          subPath: schema_defaults.conf
          readOnly: true
        ports:
        - name: http
          containerPort: 7000
      volumes:
      - name: bigquery-config
        configMap:
          name: $APP_INSTANCE_NAME-config
          items:
          - key: bigquery_conf
            path: schema_defaults.conf
