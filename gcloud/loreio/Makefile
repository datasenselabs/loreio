include ../app.Makefile
include ../crd.Makefile
include ../gcloud.Makefile
include ../var.Makefile

LORE_VERSION ?= prod
$(info ---- LORE_VERSION = $(LORE_VERSION))

TAG ?= latest
$(info ---- TAG = $(TAG))

APP_DEPLOYER_IMAGE ?= $(REGISTRY)/loreio/deployer:$(TAG)
NAME ?= loredemo
APP_PARAMETERS ?= { \
  "APP_INSTANCE_NAME": "$(NAME)", \
  "NAMESPACE": "$(NAMESPACE)" \
}
APP_INSTANCE_NAME ?= "$(NAME)"
NAMESPACE ?= $(NAMESPACE)"
APP_TEST_PARAMETERS ?= {}


submit-app:
	APP_INSTANCE_NAME=$(APP_INSTANCE_NAME) NAMESPACE=$(NAMESPACE) REGISTRY=$(REGISTRY) TAG=$(TAG) sh -ex submit_lore_app.sh

submit-devapp:
	APP_INSTANCE_NAME=$(APP_INSTANCE_NAME) NAMESPACE=$(NAMESPACE) REGISTRY=$(REGISTRY) TAG=$(TAG) sh -ex submit_lore_devapp.sh

remove-app:
	-kubectl delete -f $(APP_INSTANCE_NAME)_manifest.yaml --namespace $(NAMESPACE)

remove-data:
	-kubectl delete persistentvolumeclaims --selector app.kubernetes.io/name=$(APP_INSTANCE_NAME) --namespace $(NAMESPACE)

ssh-restapi:
	$(eval RESTAPI=$(shell kubectl get po |grep restapi |awk '{print $$1}'))
	kubectl cp dev_manifest/sshd_config  $(NAMESPACE)/$(RESTAPI):/etc/ssh/sshd_config
	kubectl exec -ti $(RESTAPI) service ssh start
	kubectl exec -ti $(RESTAPI) mkdir /root/.ssh
	kubectl exec -ti $(RESTAPI) chmod 700  /root/.ssh
	kubectl cp ~/.ssh/id_rsa.pub default/$(RESTAPI):/root/.ssh/authorized_keys
	kubectl exec -ti $(RESTAPI) chown root:root  /root/.ssh/authorized_keys
	kubectl exec -ti $(RESTAPI) chmod 640  /root/.ssh/authorized_keys


ssh-streamapi:
	$(eval STREAMAPI=$(shell kubectl get po |grep streamapi |awk '{print $$1}'))
	kubectl cp dev_manifest/sshd_config  $(NAMESPACE)/$(STREAMAPI):/etc/ssh/sshd_config
	kubectl exec -ti $(STREAMAPI) service ssh start
	kubectl exec -ti $(STREAMAPI) mkdir /root/.ssh
	kubectl exec -ti $(STREAMAPI) chmod 700  /root/.ssh
	kubectl cp ~/.ssh/id_rsa.pub default/$(STREAMAPI):/root/.ssh/authorized_keys
	kubectl exec -ti $(STREAMAPI) chown root:root  /root/.ssh/authorized_keys
	kubectl exec -ti $(STREAMAPI) chmod 640  /root/.ssh/authorized_keys

app/build:: .build/loreio/deployer \
            .build/loreio/restapi \
            .build/loreio/ui \
            .build/loreio/es \
            .build/loreio/redis \
            .build/loreio/mysql \
	    	.build/loreio/postgres \
            .build/loreio/mongo


.build/loreio: | .build
	mkdir -p "$@"


.build/loreio/deployer: deployer/* \
                           manifest/* \
                           schema.yaml \
                           .build/var/APP_DEPLOYER_IMAGE \
                           .build/var/MARKETPLACE_TOOLS_TAG \
                           .build/var/REGISTRY \
                           .build/var/TAG \
                           | .build/loreio
	docker build \
	    --build-arg REGISTRY="$(REGISTRY)/loreio" \
	    --build-arg TAG="$(TAG)" \
	    --build-arg MARKETPLACE_TOOLS_TAG="$(MARKETPLACE_TOOLS_TAG)" \
	    --tag "$(APP_DEPLOYER_IMAGE)" \
	    -f deployer/Dockerfile \
	    .
	docker push "$(APP_DEPLOYER_IMAGE)"
	@touch "$@"


.build/loreio/ui: .build/var/REGISTRY \
                            .build/var/LORE_VERSION \
                            .build/var/TAG \
                            | .build/loreio
	docker pull loreio/ui:$(LORE_VERSION)
	docker tag loreio/ui:$(LORE_VERSION) $(REGISTRY)/ui:$(TAG)
	docker push "$(REGISTRY)/ui:$(TAG)"
	@touch "$@"

.build/loreio/restapi: .build/var/REGISTRY \
                            .build/var/LORE_VERSION \
                            .build/var/TAG \
                            | .build/loreio
	docker pull loreio/restapi:$(LORE_VERSION)
	docker tag loreio/restapi:$(LORE_VERSION) $(REGISTRY)/restapi:$(TAG)
	docker push "$(REGISTRY)/restapi:$(TAG)"
	@touch "$@"

.build/loreio/redis: .build/var/REGISTRY \
                            .build/var/TAG \
                            | .build/loreio
	docker pull redis:latest
	docker tag redis:latest $(REGISTRY)/redis:$(TAG)
	docker push "$(REGISTRY)/redis:$(TAG)"
	@touch "$@"

.build/loreio/mysql: .build/var/REGISTRY \
                            .build/var/TAG \
                            | .build/loreio
	docker pull mysql:latest
	docker tag mysql:latest $(REGISTRY)/mysql:$(TAG)
	docker push "$(REGISTRY)/mysql:$(TAG)"
	@touch "$@"

.build/loreio/postgres: .build/var/REGISTRY \
                            .build/var/TAG \
                            | .build/loreio
	docker pull postgres:latest
	docker tag postgres:latest $(REGISTRY)/postgres:$(TAG)
	docker push "$(REGISTRY)/postgres:$(TAG)"
	@touch "$@"

.build/loreio/mongo: .build/var/REGISTRY \
                            .build/var/TAG \
                            | .build/loreio
	docker pull mongo:latest
	docker tag mongo:latest $(REGISTRY)/mongo:$(TAG)
	docker push "$(REGISTRY)/mongo:$(TAG)"
	@touch "$@"

.build/loreio/es: .build/var/REGISTRY \
                            .build/var/LORE_VERSION \
                            .build/var/TAG \
                            | .build/loreio
	docker pull loreio/es:prod
	docker tag loreio/es:prod $(REGISTRY)/es:$(TAG)
	docker push "$(REGISTRY)/es:$(TAG)"
	@touch "$@"
