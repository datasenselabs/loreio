#!bash
CUR_DATE=$(date +'%Y%m%d')
REGISTRY='us.gcr.io/'$(gcloud config get-value project | tr ':' '/')
NAMESPACE=kubectl config view -o jsonpath="{.contexts[?(@.name==\"$(kubectl config current-context)\")].context.namespace}"


docker pull loreio/ui:prod
docker tag loreio/ui:prod ${REGISTRY}/loreioui:$CUR_DATE
docker push ${REGISTRY}/loreioui:$CUR_DATE

docker pull loreio/restapi:prod
docker tag loreio/restapi:prod ${REGISTRY}/loreiorestapi:$CUR_DATE
docker push ${REGISTRY}/loreiorestapi:$CUR_DATE

docker pull loreio/es:prod
docker tag loreio/es:prod ${REGISTRY}/loreioes:$CUR_DATE
docker push ${REGISTRY}/loreioes:$CUR_DATE

docker pull mongo:latest
docker tag mongo:latest ${REGISTRY}/loreiomongo:$CUR_DATE
docker push ${REGISTRY}/loreiomongo:$CUR_DATE

docker pull redis:latest
docker tag redis:latest ${REGISTRY}/loreioredis:$CUR_DATE
docker push ${REGISTRY}/loreioredis:$CUR_DATE

docker pull mysql:latest
docker tag mysql:latest ${REGISTRY}/loreiomysql:$CUR_DATE
docker push ${REGISTRY}/loreiomysql:$CUR_DATE

