#!/bin/bash

if [ "${USER}" != "root" ]; then
	echo "$0 must be run as root!"
	exit 2
fi

#Menu options
options[0]="Lore Deployment on Self Managed Kubernetes Cluster" #done
options[1]="Destroy setup of Kubernetes & LoreIo Application"
options[2]="Deploy latest version of LoreIo application" #done
options[3]="LoreIO Setup & Deployment on AWS Managed Kubernetes Cluster - EKS" #done
options[4]="Scale or Remove a Node in EKS"   #0 #done
options[5]="Upgrade a kubenetes Cluster Version in EKS" #1 #done
options[6]="Scale LoreIo Application Services on new nodes"




#Actions to take based on selection
function ACTIONS {

     if [[ ${choices[0]} ]]; then
        #Option 0 selected
        echo -e "\e[1;31m Option 1 selected > Starting Now \e[0m"
        export setuptype=selfmanaged
        cd
        cd loreio/on-prem/
        #git checkout master
        #git pull origin master
        sh install_prereq.sh
        sleep 5
        mkdir -p /mnt/{mongo,es,postgres,mysql}
        make submit-pvc
        make submit-app
        sleep 120
        kubectl get po -o wide
        #git checkout feature.k8n.EKS
        #git pull origin feature.k8n.EKS


    fi

    if [[ ${choices[1]} ]]; then
        #Option 1 selected
        echo -e "\e[1;31m Option 2 selected > Starting Now \e[0m"
        read -p "Enter the setup type- EKS or SELF: " setup
        if [ $setup = EKS ]
            then 
                echo "Starting to destroy EKS Setup & LoreIo Application"
                echo -e "\e[1;33m Here is your running clusters name \e[0m" 
                eksctl get cluster
                read -p "Enter the name of EKS Cluster which you want to destroy: " clusterName
                eksctl delete cluster --name=$clusterName --wait
            else
                echo "Starting to destroy Self Managed Kubernetes Setup & LoreIo Application"
                cd
                cd loreio/on-prem/
                #git checkout master
                #git pull origin master
                sh destroy.sh
                #git checkout feature.k8n.EKS
                #git pull origin feature.k8n.EKS
        fi

    fi

    if [[ ${choices[2]} ]]; then
        #Option 2 selected
        echo -e "\e[1;31m Option 3 selected > Starting Now \e[0m"
        cd
        cd loreio/on-prem/
        read -p "Enter the Version type[prod]: " TAG
        cd
        cd loreio/on-prem/
        TAG=${TAG:-prod} make update-app
        sleep 60
    fi


    if [[ ${choices[3]} ]]; then
        #Option 3 selected
        echo -e "\e[1;31m Option 4 selected > Starting Now \e[0m"
        read -p "Enter the name(unique in a region) of EKS Cluster: " cName
        read -p "Enter the name of pemfile: " identity
        read -p "Enter the name of AWS region: " awsRegion
        read -p "Enter Access Key: " awsAccessKey
        read -p "Enter Secret Key: " awsSeretKey
        cd
        cd loreio/
        echo "Installer found >>>>> Starting"
        sh -ex LoreOneClickEKSInstaller.sh -c $cName -i $identity -r $awsRegion -a $awsAccessKey -s $awsSeretKey
        
    fi

    if [[ ${choices[4]} ]]; then
        #Option 3 selected
        echo -e "\e[1;31m Option 5 selected > Starting Now \e[0m"
        echo -e "\e[1;33m Here is your running clusters name \e[0m" 
        eksctl get cluster
        echo -e "\e[1;31m If the desired number of nodes is greater than the current maximum set to the nodeGroup then the maximum value will be increased OR decreased to match the number of requested nodes \e[0m" 
        read -p "Enter the #no of node you want to increase from the current running #no: " node
        #Adding Cluster Name input by end user , because there code be multiple cluster launch from single jump box
        read -p "Enter the name of EKS Cluster for which you want to add OR remove node: " clusterName
        export nameofNodeGroup=$(eksctl get nodegroup --cluster=$clusterName | awk 'NR == 2 {print $2}')
        eksctl scale nodegroup --cluster=$clusterName --nodes=$node --name=$nameofNodeGroup
        sleep 120
        echo -e "\e[1;34m Creating Lore Io respective DBs setup on new worker node \e[0m"
        cd
        read -p "Enter the name of pem file which were used to create this cluster: " pemFile
        export totalNR=`expr 1 + $node`
        export privateDNSIP=$(kubectl get node| awk 'NR == '$totalNR'  {print $1}')
        export nodePublicIP=$(aws ec2 describe-instances --filters "Name=private-dns-name,Values=$privateDNSIP" --query 'Reservations[*].Instances[*].PublicIpAddress' --output text)
        ssh -i $pemFile -oStrictHostKeyChecking=no ec2-user@$nodePublicIP -t  "sudo mkdir -p /mnt/{mongo,es,postgres,mysql}"
        echo "Created DB folder"

    fi

    if [[ ${choices[5]} ]]; then
        #Option 4 selected
        echo -e "\e[1;31m Option 6 selected > Starting Now \e[0m"
        echo -e "\e[1;33m Here is your running clusters name \e[0m" 
        eksctl get cluster
        read -p "Enter the name of EKS Cluster which you want to upgrade: " clusterName
        eksctl update cluster --name=$clusterName --approve
    fi


    if [[ ${choices[6]} ]]; then
        #Option 4 selected
        echo -e "\e[1;31m Option 7 selected > Starting Now \e[0m"
        echo -e "\e[1;33m Here is your deployed pods on cluster \e[0m" 
        kubectl get pods -o wide
        echo -e "\e[1;33m Here is the Name of LoreIo application Services {'cust-restapi' 'cust-streamapi' 'cust-ui'} \e[0m"
        read -p "Enter the name of LoreIo Application service , which you want to scale: " appService
        read -p "Enter the #no of replicas which you want to deploy for above service: " noReplicas
        kubectl scale --replicas=$noReplicas deployment/$appService
    fi

}

#Variables
ERROR=" "

#Clear screen for menu
clear

#Menu function
function MENU { 
    echo -e "\e[1;31m Choose Options to Facilitate LoreIO Services \e[0m"
    for NUM in ${!options[@]}; do
        echo "[""${choices[NUM]:- }""]" $(( NUM+1 ))") ${options[NUM]}"
    done
    echo "$ERROR"
}

#Menu loop
while MENU && read -e -p "Select the desired options using their number (again to uncheck, ENTER when done): " -n1 SELECTION && [[ -n "$SELECTION" ]]; do
    clear
    if [[ "$SELECTION" == *[[:digit:]]* && $SELECTION -ge 1 && $SELECTION -le ${#options[@]} ]]; then
        (( SELECTION-- ))
        if [[ "${choices[SELECTION]}" == "+" ]]; then
            choices[SELECTION]=""
        else
            choices[SELECTION]="+"
        fi
            ERROR=" "
    else
        ERROR="Invalid option: $SELECTION"
    fi
done

ACTIONS